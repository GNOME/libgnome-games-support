# Abkhazian translation for libgnome-games-support.
# Copyright (C) 2022 libgnome-games-support's COPYRIGHT HOLDER
# This file is distributed under the same license as the libgnome-games-support package.
# Нанба Наала <naala-nanba@rambler.ru>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnome-games-support master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libgnome-games-support/"
"issues\n"
"POT-Creation-Date: 2022-02-15 22:53+0000\n"
"PO-Revision-Date: 2022-02-15 22:53+0000\n"
"Last-Translator: Нанба Наала <naala-nanba@rambler.ru>\n"
"Language-Team: Abkhazian <ab@li.org>\n"
"Language: ab\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Appears at the top of the dialog, as the heading of the dialog
#: games/scores/dialog.vala:82
msgid "Congratulations!"
msgstr "Ишәыдаҳныҳәалоит!"

#: games/scores/dialog.vala:84
msgid "High Scores"
msgstr "Иреиӷьу алҵшәақәа"

#: games/scores/dialog.vala:86
msgid "Best Times"
msgstr "Иреиӷьу аамҭа"

#: games/scores/dialog.vala:105
msgid "No scores yet"
msgstr "Макьаана акәаталақәа ыҟаӡам"

#: games/scores/dialog.vala:109
msgid "Play some games and your scores will show up here."
msgstr "Ԥыҭк ахәмаррақәа шәаныхәмарлак ашьҭахь, шәкәаталақәа абраҟа ицәырҵоит."

#. A column heading in the scores dialog
#: games/scores/dialog.vala:166
msgid "Rank"
msgstr "Аранг"

#. A column heading in the scores dialog
#: games/scores/dialog.vala:175
msgid "Score"
msgstr "Акәаталақәа"

#: games/scores/dialog.vala:177
msgid "Time"
msgstr "Аамҭа"

#. A column heading in the scores dialog
#: games/scores/dialog.vala:184
msgid "Player"
msgstr "Ахәмарҩы"

#. Appears on the top right corner of the dialog. Clicking the button closes the dialog.
#: games/scores/dialog.vala:194
msgid "_Done"
msgstr "_Ихиоуп"

#. Time which may be displayed on a scores dialog.
#: games/scores/dialog.vala:308
#, c-format
msgid "%ld minute"
msgid_plural "%ld minutes"
msgstr[0] "%ld минуҭк" 
msgstr[1] "%ld минуҭ" 

#. Time which may be displayed on a scores dialog.
#: games/scores/dialog.vala:310
#, c-format
msgid "%ld second"
msgid_plural "%ld seconds"
msgstr[0] "%ld секундк" 
msgstr[1] "%ld секунд"

#: games/scores/dialog.vala:317
msgid "Your score is the best!"
msgstr "Шәара шәылҵшәа зегьы иреиӷьуп!"

#: games/scores/dialog.vala:319
msgid "Your score has made the top ten."
msgstr "Шәара шәылҵшәа  иреиӷьу жәаба рахь иалалеит."
